import os
import csv
import re
import sys
import time
from datetime import timedelta

mode = None 

instrucciones = """ INSTRUCCIONES
Nota: si hay alguna entrada de tiempo rara en los csv,
 saltará un assertion (comprueba que el tini < tfinal,
  que son timestamps posibles, etc.)

0) Si modificas algo del cryptopad sheet, copias las celdas a un
libreoffice calc, y guardas como csv.
- Importa el nombre de los vídeos en el csv


1) Si no tienes los vídeos descargados, descarga los vídeos haciendo: 
$ cd 00_VideosRaw
$ source download_videos.sh

2) El script parsea el contenido de cada sesión en el
contenido de los ".csv". Luego busca en el directorio
de los vídeos el video "source" o "parent"y si lo 
encuentra, generará un comando ffmpeg.

-----------[IMPORTANTE -> SE ESPERA UN DIRECTORY TREE ASÍ]----------:

├── [00_VideosRaw]      <----- Carpeta con los vídeos descargados y los csv, el nombre importa!
│   ├── download_videos.sh      (bash script para descargar los videos, necesita "lista_videos txt")
│   ├── lista_sabado.csv        
│   ├── lista_videos.txt        (lista de URLs a los videos para descargar)
│   ├── Sabado-00-Pista-Plenaria.mp4     
│   ├── Sabado-01-Pista-I.mp4
│   ├── Sabado-02-Pista-II.mp4
│   ├── Sabado-03-Pista-III.mp4
│   ├── Sabado-04-Haskell.mp4
│   ├── Sabado-05-Andalugeeks.mp4
│   ├── Viernes-00-Pista-Plenaria.mp4
│   ├── Viernes-01-Pista-II.mp4
│   ├── Viernes-01-Pista-I.mp4
│   ├── Viernes-03-Cultura-Libre.mp4
│   ├── Viernes-04-Katas-Haskell.mp4
│   ├── Viernes-05-Derechos-Digitales.mp4
│   └── Viernes-06-Sala-Gnome.mp4
|
├── [01_VideosOutput]   <------ Aquí salen los videos 
├── main_eslibre.py
------------------------------------------------------------------
"""



video_parent_names = [
    "Sabado-00-Pista-Plenaria.mp4",
    "Sabado-01-Pista-I.mp4",
    "Sabado-02-Pista-II.mp4",
    "Sabado-03-Pista-III.mp4",
    "Sabado-04-Haskell.mp4",
    "Sabado-05-Andalugeeks.mp4",
    "Viernes-00-Pista-Plenaria.mp4",
    "Viernes-01-Pista-I.mp4",
    "Viernes-01-Pista-II.mp4" ,
    "Viernes-03-Cultura-Libre.mp4",
    "Viernes-04-Katas-Haskell.mp4" ,
    "Viernes-05-Derechos-Digitales.mp4" ,
    "Viernes-06-Sala-Gnome.mp4" 
    ]

absolute_path_parent_videos = []



def create_cmd_corta_video(v_info):
    #ffmpeg -ss 00:00:00 -i Viernes-05-Derechos-Digitales.mp4 -t 00:08:54 -vcodec copy -acodec copy temp/00_Presentacion.mp4

    init = v_info.init
    diff  = v_info.diff

    pth2_v_In = v_info.path2Parent
    pth2_v_Out = v_info.path2OutFile
    
    if ( pth2_v_In is None or  pth2_v_Out is None or
        init is None or diff is None):
        print_c("ERROR: algún campo está faltando:" ,bclr.FAIL )
        v_info.print_fields()
        return -1

    cmd = f"ffmpeg -ss {init} -i {pth2_v_In} -t {diff} -vcodec copy -acodec copy {pth2_v_Out}"
    #cmd = f"ffmpeg -i {pth2_v_In} -ss {init} -to {}

    return cmd



#MAIN FUNCTION
def do_main():
    global absolute_path_parent_videos
    global mode
    currDir = os.getcwd()
    print("Curr Dir:" + currDir)

    listOfFiles= list_all_files_by_ending(currDir, ".csv")

    absolute_path_parent_videos = find_abs_paths_parent_videos("00_VideosRaw")

    dia_eslibre = []
    # para cada .csv (uno del viernes, otro del sabado), parsea la informacion
    # de cada sesion
    for csv in listOfFiles:
        list_of_videos = parse_csv_file(csv)
        
        dia_eslibre.append(list_of_videos)

    cmd_list = []
    
    # para cada dia de sesiones parseada, para cada info de cada sesion,
    # genera el comando de cortar según los tiempos

    for sesiones_del_dia in dia_eslibre:
        for video_info in sesiones_del_dia:
            cmd_list.append(create_cmd_corta_video(video_info))

    print_c("MODE IS : {}".format(mode),bclr.FAIL)

    if len(cmd_list) > 0:
        if mode == "CMD":
            for cmd in cmd_list:
                os.system(cmd)

            pass
        elif mode == "JUST_ONE":
            os.system(cmd_list[0])

            pass

        else:
            count = 0
            for cmd in cmd_list:
                count +=1
                print_c("cmd {}: {}".format(count,cmd),bclr.OKGREEN)
                time.sleep(0.2)
    
    else:
        print_c("ERROR: no se ha generado ningúng comando ",bclr.FAIL)









#  Para hacer algunos prints en color para uqe sea legible
class bclr:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_c(myStr, clr):
    print(clr + myStr + bclr.ENDC)


# Funciones para checkear los timestamps y calcular los diffs
def is_timestamp(timestr):
    # comprueba si es un timestamp o no
    pattern = re.compile("(\d{1,2}:\d{2}:\d{2})")
    #print(timestr)
    m = pattern.search(timestr)

    if m:
        return True
    else:
        return False


def timestamp2sec(timestr):    
    h, m, s = timestr.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)    

def sec2timestamp(total_sec):
    return "{:0>8}".format(str(timedelta(seconds=total_sec)))



def time_diff(tfin,tini):
    # recibe dos strings timestamp tipo 03:34:15 o 3:34:15 
    # -> devuelve otra string con la diferencia de tiempo 

    tfin_s = timestamp2sec(tfin)
    tini_s = timestamp2sec(tini)

    diff = tfin_s - tini_s

    assert diff>0, "ERROR: time_diff, {} no es mayor que {} !!!".format(tfin,tini)
    assert diff < 10*60*60, "ERROR: time_diff: {} seconds demasiado grande!".format(diff)

    return sec2timestamp(diff)



# cada video que parseamos es un objeto de esta clase con la informacion sobre el video que produciremos
class MetaVideo():
    def __init__ (self,line,parent):
        # filtra renglones malos
        for field in line[:3]:
            if field == ' ':
                return None        
        
        self.init = None
        self.diff = None
        self.end  = None
        self.parentName = None
        self.title = None
        self.path2Parent = None
        self.path2OutFile = None

        self.parentName = parent
        
        self.set_title(line[1]) 
        self.set_init_end(line)
        self.set_path2Parent()
        self.set_path2OutFile()


    def set_path2Parent (self):    
        for pth2file in absolute_path_parent_videos:
            if self.parentName in pth2file:
                self.path2Parent =  pth2file
    
    def set_path2OutFile(self):
        outDir = os.path.join(os.getcwd(),"01_VideosOutput")
        self.path2OutFile = os.path.join(outDir,self.title+".mp4")        

    def set_title(self,big_title):
        aux = big_title.replace(' ', '_')
        aux = aux.replace("(",'_')
        aux = aux.replace(")",'_')
        aux = aux.replace(',','_')
        if len(aux) > 25:
            aux = aux[:24]
        
        self.title = self.parentName.replace('.mp4','_') + aux

    def set_init_end(self,line):
        #este paso es para estandarizar el formato de 03:34:35 y 3:34:35 a 03:34:35
        init = line[2]
        end = line[3]

        self.init = sec2timestamp(timestamp2sec(init))
        self.end = sec2timestamp(timestamp2sec(end))
        self.diff = time_diff(end,init)
    
    def print_fields(self):
        
        print_c("Video title: "+ self.title,bclr.OKBLUE)
        print_c("t_ini: {} \nt_end: {}\n diff: {}".format(self.init, self.end,self.diff), bclr.OKBLUE)
        print_c("path2parent: "+self.path2Parent,bclr.OKBLUE)
        print_c("path2OutFile: "+ self.path2OutFile, bclr.OKBLUE)

  
   
        


def parse_csv_file(pth2file):
    file=open( pth2file, "r")
    reader = csv.reader(file)

    last_parent = None
    parent_v = None

    list_video_info= []

    for line in reader:
        
        aux = line[0]
        if aux != '':
            print (line)
            #buscando el parent video
            if aux in video_parent_names:
                print_c(aux,bclr.OKBLUE)
                parent_v = aux
            
            if parent_v is not None and is_timestamp(line[2]) and is_timestamp(line[3]):
                
                newVideo = MetaVideo(line,parent_v) 
                if newVideo is not None:
                    newVideo.print_fields()
                    print_c("......................",bclr.HEADER)     
                    list_video_info.append(newVideo)

        last_parent = parent_v

    return list_video_info
    


def list_all_files_by_ending(rootDir,ending):

    listOfFiles = []
    for root, dirs, files in os.walk(rootDir):
        for file in files:
        

            if file.endswith(ending):
                print_c(os.path.join(root, file),bclr.OKGREEN)
                listOfFiles.append(os.path.join(root, file))
    
    return listOfFiles


def find_abs_paths_parent_videos(rootDir):
    listOfFiles = []

    raw_dir = os.path.join(os.getcwd(),rootDir)

    for root, dirs, files in os.walk(raw_dir):

        for file in files:
            if file.endswith('.mp4') and file in video_parent_names:
                print_c(os.path.join(root, file),bclr.HEADER)
                listOfFiles.append(os.path.join(root, file))
                pass
    
    for name in video_parent_names:
        for path2video in listOfFiles:
            if name in path2video:
                print_c("-[x] "+name+" --> OK!",bclr.WARNING) 
                

    return listOfFiles




    #absoluteFilePaths(currDir)

    # traverse root directory, and list directories as dirs and files as files
    #for root, dirs, files in os.walk("."):
        #path = root.split(os.sep)
        #print((len(path) - 1) * '---', os.path.basename(root))
    #    for file in files:
    #        print(file)
            #print(len(path) * '---', file)



def test_is_timestamp():
    print("TESTEANDO is_timestamp! ... ")
    testnum  = 0
    testnum += 1
    print("TEST {}".format(testnum))
    line = "3:45:34"
    res = is_timestamp(line)
    assert res == True, "Error, 3:45:34 SI que es un timestamp, returned {}".format(res)
    print("PASSED TEST {}".format(testnum))

    testnum += 1
    print("TEST {}".format(testnum))
    line = "03:5:34"
    res = is_timestamp(line)
    assert res == False, "Error, {} NO es un timestamp, returned {}".format(line,res)
    print("PASSED TEST {}".format(testnum))


    testnum += 1
    print("TEST {}".format(testnum))
    line = "03:56:11"
    res = is_timestamp(line)
    assert res == True, "Error, {} SI es un timestamp, returned {}".format(line,res)
    print("PASSED TEST {}".format(testnum))

    testnum += 1
    print("TEST {}".format(testnum))
    line = "me llamo ralph"
    res = is_timestamp(line)
    assert res == False, "Error, {} NO es un timestamp, returned {}".format(line,res)
    print("PASSED TEST {}".format(testnum))

    print("leave TESTEANDO is_timestamp() ...")


def parse_argvs ():

    global mode
        
    if mode != "FORCE":
        mode = "PRINT"
        if len(sys.argv) > 1:
            if sys.argv[1] == "-todos" :
                mode = "CMD"
            elif sys.argv[1] == "-test":
                mode = "TEST"
            elif sys.argv[1] == "-uno":
                mode = "JUST_ONE"
            elif sys.argv[1] == "-help" or sys.argv[1] == "-h" :
                print_c("help, estos son los commands del script: \nno arg          -> print de los commands ffmpeg que se harían",bclr.WARNING)
                print_c("arg == '-todos' -> realiza todos los comandos ffmpeg generados",bclr.WARNING)
                print_c("arg == '-uno'   -> se realiza solo el primer comando ffmpeg generado",bclr.WARNING)
                print_c("arg == '-test'  -> corre un par de tests, nada interesante",bclr.WARNING)
                print("----")
                print("----")
                print_c(instrucciones, bclr.WARNING)
                sys.exit(0)





if __name__ == "__main__":    

    parse_argvs()

    if mode == "TEST":
        testeando = True
    else:
        testeando = False



    if testeando:
        
        print_c("DISABLE testeando vadiable ...",bclr.OKBLUE)

        test_is_timestamp()
        print("estamos testeando, exit!")
        print(time_diff("12:35:34", "12:35:00"))
        print(time_diff("03:35:34", "2:35:00"))


        find_abs_paths_parent_videos('00_VideosRaw')

        sys.exit(-1)

    else:
        do_main()
